@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="float-right">
                    <add-project-button></add-project-button>
                </div>
                <h1>{{ __('Projects') }}</h1>
                <projects-list :team="{{ auth()->user()->own_team }}"></projects-list>
            </div>
        </div>
    </div>
@endsection
