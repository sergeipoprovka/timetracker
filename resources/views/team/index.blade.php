@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="float-right">
                    <add-team-member></add-team-member>
                </div>
                <h2>{{__('My team')}}</h2>
                <div class="clearfix"></div>
                <br>
                <div class="card">
                    <div class="card-body">
                        @if(auth()->user()->has('teams'))
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>{{__('User')}}</th>
                                    <th>{{__('Date')}}</th>
                                    <th>{{__('Status')}}</th>
                                    <th></th>
                                </tr>
                                @foreach(auth()->user()->own_team->members as $member)
                                    @if(auth()->user()->id != $member->id)
                                        <tr>
                                            <td>{{ $member->full_name }}</td>
                                            <td>{{ $member->created_at->diffForHumans() }}</td>
                                            <td><div class="badge badge-success">{{__('Member')}}</div></td>
                                            <td>
                                                <form style="display: inline-block" method="post" action="/dashboard/team/{{ $member->id }}">
                                                    {{ method_field('DELETE') }}
                                                    @csrf
                                                    <button class="btn btn-danger btn-sm">
                                                        <ion-icon name="trash"></ion-icon>
                                                    </button>
                                                </form>

                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </table>
                        @endif
                    </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-body">
                        <h3>{{__('Pending invites')}}</h3>
                        @if($invitations->count())
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th>{{__('User')}}</th>
                                    <th>{{__('Date')}}</th>
                                    <th>{{__('Status')}}</th>
                                    <th></th>
                                </tr>
                                @foreach($invitations as $invitation)
                                    <tr>
                                         <td>@if($invitation->user) {{ $invitation->user->full_name }} @else {{ $invitation->email }} @endif</td>
                                        <td>{{ $invitation->created_at->diffForHumans() }}</td>
                                        <td><div class="badge badge-info">{{__('Pending')}}</div></td>
                                        <td>
                                            <form style="display: inline-block" method="post" action="/dashboard/invite/{{ $invitation->id }}">
                                                {{ method_field('DELETE') }}
                                                @csrf
                                                <button class="btn btn-danger btn-sm">
                                                    <ion-icon name="trash"></ion-icon>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
