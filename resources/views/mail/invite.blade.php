<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nice invitation from Mr. Mailing Robot</title>
    <style>
        .btn{
            padding: 10px 40px;
            font-size:14px;
            border-radius: 5px;
            display: inline-block;
            margin-right: 10px;
        }
        .accept{
            background-color: green;
            color: white;
        }
        .deny{
            background-color: red;
            color: white;
        }
    </style>
</head>
<body>
    <h1>Invitation arrived!</h1>
    <p>User {{ $user_invited->profile->firstname . " " . $user_invited->profile->lastname }}
        has invited you to join his timetracker.</p>
    <p>
    <p>
        <a class="btn accept" href="{{ env('APP_URL') }}/invite/{{ $mailhash }}/accept">Accept invite</a>
        <a class="btn deny" href="{{ env('APP_URL') }}/invite/{{ $mailhash }}/deny">Deny invite</a>
    </p>
        Sincerely yours,<br>
        Your neighbour,<br>
        MailBot.
    </p>
</body>
</html>
