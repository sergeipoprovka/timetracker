@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                @include('admin.elements.timeframe_filter')
                <br>
                <h2>{{__('Timeframes')}}</h2>
                @if($timeframes->count())
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th>{{__('User')}}</th>
                            <th>{{__('Project')}}</th>
                            <th>{{__('Task')}}</th>
                            <th>{{__('Duration')}}</th>
                            <th>{{__('Actions')}}</th>
                        </tr>
                        @foreach($timeframes as $timeframe)
                            <tr>
                                <td>{{ $timeframe->user->full_name }}</td>
                                <td>@if($timeframe->task && $timeframe->task->project) {{ $timeframe->task->project->name }} @endif</td>
                                <td>@if($timeframe->task) {{ $timeframe->task->name }} @endif</td>
                                <td>{{ $timeframe->duration }}</td>
                                <td>
                                    <a href="/admin/timeframes/{{ $timeframe->id }}" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                                    <a href="/admin/timeframes/{{ $timeframe->id }}/edit" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a>
                                    <form method="post" action="/admin/timeframes/{{ $timeframe->id }}" style="display: inline;">
                                        @csrf
                                        {{ method_field('DELETE') }}
                                        <button class="btn btn-danger btn-sm"><i class="fa fa-close"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    <div class="pagination">
                        {{ $timeframes->appends(request()->input())->links() }}
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection