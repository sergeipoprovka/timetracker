<div class="card">
    <div class="card-body">
        <form method="get" action="/admin/timeframes">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <select class="form-control" name="user" id="user">
                            <option value="" selected disabled>{{__('Select user')}}</option>
                            @foreach($users as $user=>$id)
                                <option value="{{ $id }}">{{ $user }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <select class="form-control" name="project" id="project">
                            <option value="" selected disabled>{{__('Select project')}}</option>
                            @foreach($projects as $project=>$id)
                                <option value="{{ $id }}">{{ $project }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <select class="form-control" name="task" id="task">
                            <option value="" selected disabled>{{__('Select task')}}</option>
                            @foreach($tasks as $task=>$id)
                                <option value="{{ $id }}">{{ $task }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <select class="form-control" name="duration" id="duration">
                            <option value="" selected disabled>{{__('Select duration')}}</option>
                            <option value="1">Less than minute</option>
                            <option value="30">Less than 30 minutes</option>
                            <option value="60">Less than 1 hour</option>
                            <option value="61">More than 1 hour</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                <div class="form-group">
                    <button type="submit" class="btn btn-success">{{__('Filter')}}</button>
                </div>
            </div>
            </div>
        </form>
    </div>
</div>
