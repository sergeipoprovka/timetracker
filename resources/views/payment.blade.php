@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>{{ __('Payment') }}</h1>
                <div class="card">
                    <div class="card-body">
                        <subscription></subscription>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('header_scripts')
    <script src="https://js.stripe.com/v3/"></script>
@endsection












