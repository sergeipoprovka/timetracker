
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component('auth', require('./components/Auth'));
Vue.component('login', require('./components/Login'));
Vue.component('register', require('./components/Register'));
Vue.component('payment', require('./components/Payment'));
Vue.component('subscription', require('./components/Subscription'));
Vue.component('subscription-chooser', require('./components/SubscriptionChooser'));
Vue.component('add-project-button', require('./components/AddProjectButton'));
Vue.component('projects-list', require('./components/ProjectsList'));
Vue.component('form-handler', require('./components/FormHandler'));
Vue.component('add-project-form', require('./components/AddProjectForm'));
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('header-timer', require('./components/HeaderTimer'));
Vue.component('add-team-member', require('./components/AddTeamMember'));

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const EventBus = new Vue();
export default EventBus

const app = new Vue({
    el: '#app',
    mounted(){
        let userid = document.head.querySelector('meta[name="user-id"]').getAttribute('content');
        Pusher.logToConsole = true
        Echo.channel('AddMemberToTeam-' + userid)
            .listen('AddMemberToTeam', (event)=>{
                $('#app').after('<div class="alert alert-info alert-dismissible notification"><button type="button" class="close" data-dismiss="alert">&times;</button>You\'ve been invited to the team. <a href="#">Go to invitation</a></div>')
                setTimeout(function(){
                    $('.notification').fadeOut()
                    setTimeout(function(){
                        $('.notification').remove()
                    },3000)
                }, 10000)
            })
    }
});
















