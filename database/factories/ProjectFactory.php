<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Project::class, function (Faker $faker) {
    return [
        'category_id'=>$faker->numberBetween(1,50),
        'name'=>$faker->streetName,
        'estimation'=>$faker->numberBetween(100, 10000),
        'billable'=>$faker->boolean
    ];
});
