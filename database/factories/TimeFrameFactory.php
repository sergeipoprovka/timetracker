<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\TimeFrame::class, function (Faker $faker) {
    return [
        'started_at'=>$faker->unixTime,
        'finished_at'=>$faker->unixTime,
    ];
});
