<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Task::class, function (Faker $faker) {
    return [
        'name'=>$faker->streetName,
        'description'=>$faker->realText(200),
        'billable'=>$faker->boolean
    ];
});
