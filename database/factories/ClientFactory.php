<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Client::class, function (Faker $faker) {
    return [
        'name'=>$faker->streetName,
        'contact_person'=> $faker->lastName." ".$faker->lastName,
        'contact_email'=>$faker->email,
        'contact_phone'=>$faker->phoneNumber
    ];
});
