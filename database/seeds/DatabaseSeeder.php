<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Category::class, 50)->create();
        $tags = factory(\App\Models\Tag::class, 150)->create();
        factory(\App\Models\User::class, 10)->create()->each(function($user){
            $user->profile()->save(factory(\App\Models\Profile::class)->make());
            $team = $user->own_team()->save(factory(\App\Models\Team::class)->make());
            $user->teams()->attach($team);
            $user->teams()->each(function($team) use ($user){
               $team->clients()->saveMany(factory(\App\Models\Client::class,3)->make());
               $team->clients()->each(function($client) use ($team, $user){
                   $client->projects()->saveMany(factory(\App\Models\Project::class, 3)->make(['team_id'=>$team->id]));
                   $client->projects()->each(function($project) use ($user){
                       $project->tags()->attach($this->giveMeRandomArray(1,150, rand(3,7)));
                       $project->tasks()->saveMany(factory(\App\Models\Task::class, 5)->make());
                       $project->tasks()->each(function($task) use ($user){
                          $task->timeframes()->saveMany(factory(\App\Models\TimeFrame::class, 5)->make(['user_id'=>$user->id]));
                       });
                   });
               });
            });
        });
    }

    private function giveMeRandomArray($start, $end, $elements){
        $array = [];
        for($i=0;$i<$elements;$i+=1){
            $array[] = rand($start, $end);
        }
        return $array;
    }
}










