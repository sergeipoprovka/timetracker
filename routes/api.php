<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix'=>'v2','namespace'=>'Api'],function(){
    Route::get('categories','RestController@getCategories');
    Route::get('projects/{team}','RestController@getProjects');
    Route::get('tasks/{project}','RestController@getTasks');
    Route::get('allprojects/{team}/{client?}','RestController@getAllProjects');
    Route::get('clients/{team}','RestController@getClients');
    Route::post('getUsernameSuggestions','RestController@getUsernameSuggestions');
});
















