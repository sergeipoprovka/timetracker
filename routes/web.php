<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

// AUTH ROUTES
Route::get('login', 'AuthController@showLoginForm')->name('login');
Route::post('login', 'AuthController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('register', 'AuthController@showLoginForm')->name('register');
Route::post('register', 'AuthController@register');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
//AUTH ROUTES END

Route::group(['prefix'=>'dashboard','middleware'=>['auth','payed']], function(){
    Route::get('/','DashboardController@index');
    Route::resource('projects','ProjectController');
    Route::resource('tasks','TaskController')->only('store');
    Route::resource('team','TeamController')->only('index','store');
    Route::delete('team/{user}','TeamController@destroy');
    Route::resource('timeframes','TimeframeController')->except('index','show','create','edit');
    Route::delete('invite/{invite}','TeamController@destroy_invite');
});

Route::group(['prefix'=>'admin','middleware'=>['auth','locale'],'namespace'=>'Admin'], function(){
    Route::resource('timeframes','TimeFrameController');
});

Route::get('/invite/{mailhash}/{action}','TeamController@handleInvitation')->middleware('auth');
Route::get('payment','PaymentController@index');
Route::post('subscription/{plan}','PaymentController@subscription');

Route::get('setLocale/{locale}','HomeController@setLocale');




















