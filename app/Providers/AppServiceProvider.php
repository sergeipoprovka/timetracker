<?php

namespace App\Providers;

use App\Models\TimeFrame;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        View::composer("*", function ($view) {
            if(auth()->check()) {
                $timeframe = TimeFrame::where('user_id','=',auth()->user()->id)
                    ->whereNull('finished_at')
                    ->orderBy('id','DESC')
                    ->with('task')
                    ->first();
                $view->with('current_timeframe',$timeframe);
            }
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
