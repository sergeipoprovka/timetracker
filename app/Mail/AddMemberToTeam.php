<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddMemberToTeam extends Mailable
{
    use Queueable, SerializesModels;

    private $invitee;
    private $mailhash;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($invitee, $mailhash)
    {
        $this->invitee = $invitee;
        $this->mailhash = $mailhash;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.invite')->with([
            'invitee'=>$this->invitee,
            'user_invited'=>auth()->user(),
            'mailhash'=>$this->mailhash
        ]);
    }
}
