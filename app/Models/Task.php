<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['project_id','name','description','billable'];

    public function timeframes(){
        return $this->hasMany(TimeFrame::class);
    }

    public function project(){
        return $this->belongsTo(Project::class)->with('client');
    }
}
