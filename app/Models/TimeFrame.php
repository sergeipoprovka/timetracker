<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TimeFrame extends Model
{
    protected $fillable = ['user_id','started_at','finished_at','task_id'];

    protected $attributes = ['duration','difference'];

    public function task(){
        return $this->belongsTo(Task::class)->with('project');
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function getDurationAttribute(){
        if($this->finished_at) {
            $diff = abs($this->finished_at - $this->started_at);
            $hrs = floor(abs($diff) / 3600);
            $min = floor((abs($diff) - $hrs * 3600) / 60);
            $sec = abs($diff) - ($hrs * 3600) - ($min * 60);
            $fullTime = sprintf('%02d', $hrs) . ":" . sprintf('%02d', $min) . ":" . sprintf('%02d', $sec);
            return ($this->finished_at > $this->started_at) ? $fullTime : "-" . $fullTime;
        }
        return "In process";
    }

    public function getDifferenceAttribute(){
        return abs($this->finished_at - $this->started_at);
    }
}















