<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['team_id','client_id','category_id','billable','estimation','name'];

    public function tasks(){
        return $this->hasMany(Task::class);
    }

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function tags(){
        return $this->belongsToMany(Tag::class);
    }
}
