<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function index(){
        return view('payment');
    }

    public function subscription(Request $request, $plan){
        $token = $request->stripeToken;
        $subscr = auth()->user()->newSubscription('main', $plan)->create($token);
        if($subscr){
            $stripeCustomer = auth()->user()->asStripeCustomer();
            $lastSubscription = $stripeCustomer->subscriptions['data'][count($stripeCustomer->subscriptions['data']) - 1];

            if($lastSubscription->status == "active"){
                auth()->user()->subscription_period_start = $lastSubscription->current_period_start;
                auth()->user()->subscription_period_end = $lastSubscription->current_period_end;
                auth()->user()->subscription_active = 1;
                if(auth()->user()->save()){
                    return "1";
                }
            }
        }
        return "0";
    }

//    TODO: Handle subscription cancellation

}















