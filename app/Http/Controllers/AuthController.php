<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Models\Team;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function showLoginForm(){
        $referer = app('Illuminate\Routing\UrlGenerator')->previous();
        $redirectTo = strstr($referer,'invite') ? $referer : "/dashboard";
        if(Auth::check()){
            return redirect('/');
        }else{
            return view('auth', ['redirect_url'=>$redirectTo]);
        }
    }

    public function login(Request $request){
        $request->validate([
            'email'=>'required|email',
            'password'=>'required|string'
        ]);

        if(Auth::attempt(['email'=>$request->email,'password'=>$request->password])){
            return response()->json(['id'=>auth()->user()->id], 200);
        }

        return response()->json(
            ['errors'=> [
                'email'=>['There is no user with this E-mail address or password']
            ]
            ], 419);
    }

    public function register(Request $request){
        $request->validate([
           'firstname'=>'required|string',
           'lastname'=>'required|string',
           'email'=>'required|email|unique:users',
           'password'=>'required|string|confirmed'
        ]);

        $user = User::create([
            'email'=>$request->email,
            'password'=>bcrypt($request->password),
            'role'=> env('DEFAULT_ROLE')
        ]);

        if($user){
            $profile = Profile::create([
               'user_id'=>$user->id,
               'firstname'=>$request->firstname,
               'lastname'=>$request->lastname
            ]);

            if($profile){
                $team = Team::create([
                    'owner_id'=>$user->id
                ]);
                if($team) {
                    $team->members()->attach($user);
                    return $this->login($request);
                }
            }else{
                if(User::destroy($user->id)){
                    return response()->json(['errors'=>[
                        'general'=>['Something went wrong...Try again yesterday.']
                    ]],419);
                }
            }
        }

        return response()->json(['errors'=>[
            'general'=>['Something went wrong...Try again today.']
        ]],419);
    }
}













