<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use App\Models\Client;
use App\Models\Team;
use App\Models\Task;
use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RestController extends Controller
{
    public function getCategories(){
        return Category::all();
    }

    public function getClients(Team $team){
        return Client::where([['team_id', '=', $team->id]])->get();
    }

    public function getProjects(Team $team){
        $projects = Project::where('team_id','=', $team->id)->orderBy('id','DESC')->with('client')->paginate(5);
        return json_encode($projects);
    }

    public function getTasks(Project $project){
        $tasks = Task::where('project_id','=', $project->id)->get();
        return json_encode($tasks);
    }

    public function getAllProjects(Team $team, Client $client){
        $where[] = ['team_id','=', $team->id];
        if($client->id != null){
            $where[] = ['client_id','=', $client->id];
        }
        $projects = Project::where($where)->orderBy('id','DESC')->with('client')->get();
        return json_encode($projects);
    }

    public function getUsernameSuggestions(Request $request){
        return User::select('email')->where('email','LIKE',$request->querystring.'%')->first();
    }
}













