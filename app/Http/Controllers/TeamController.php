<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Team;
use App\Models\Invite;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    public function index(){
        $invitations = Invite::where('team_id','=',auth()->user()->own_team->id)->get();
        return view('team.index', compact('invitations'));
    }

    public function store(Request $request){
        $request->validate([
            'email'=>'required|email'
        ]);
        $user = User::select('id','email')->where('email','=',$request->email)->first();
        if(!$user){
            $user = new \stdClass();
            $user->email = $request->email;
        }
        $mailhash = $this->createHash(40);
        $invite = Invite::create([
            'user_id'=> isset($user->id) ? $user->id : NULL,
            'team_id'=>auth()->user()->own_team->id,
            'email'=> $user->email,
            'hash'=>$mailhash
        ]);
        event(new \App\Events\AddMemberToTeam($user, $mailhash));

        // TODO : Return SUCCESS
    }

    private function createHash($qty){
        $hash = [];
        $alphas = array_merge(range('A', 'Z'), range('a', 'z'), range(0,9));
        for($i=0;$i<$qty;$i+=1){
            $hash[] = $alphas[rand(0,count($alphas) - 1)];
        }
        return implode("", $hash);
    }

    public function handleInvitation($mailhash, $action){
        $invite = Invite::where('hash','=',$mailhash)->first();
        if($invite){
            $this->makeInvite($invite, $action);
        }else{
            abort('403');
        }
        return redirect('/');
    }

    private function makeInvite(Invite $invite, $action){
        if ($action == "accept") {
            if($invite->user_id) {
                $user = $invite->user_id;
            }else{
                $u = User::where('email','=',$invite->email)->first();
                $user = $u ? $u->id : NULL;
            }
            if($user){
                $team = Team::find($invite->team_id);
                $team->members()->attach($user);
                $invite->destroy($invite->id);
            }
        }
    }

    public function destroy(User $user){
        if(auth()->user()->own_team->members()->detach($user->id)){
            return redirect()->back();
        }
    }

    public function destroy_invite(Invite $invite){
        if(Invite::destroy($invite->id)){
            return redirect()->back();
        }
    }
}












