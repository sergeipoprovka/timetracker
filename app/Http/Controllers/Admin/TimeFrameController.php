<?php

namespace App\Http\Controllers\Admin;

use App\Models\Project;
use App\Models\Task;
use App\Models\TimeFrame;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TimeFrameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::all()->pluck('id','full_name');
        $projects = Project::all()->pluck('id','name');
        $tasks = Task::all()->pluck('id','name');

        $base = TimeFrame::select('*');

        $where = [];
        if($request->input('user')){
            $where[] = ['user_id','=',$request->input('user')];
        }
        if($request->input('task')){
            $where[] = ['task_id','=',$request->input('task')];
        }
        if($request->input('project')){
            $base->whereHas('task', function($q) use ($request){
                $q->where('project_id','=',$request->input('project'));
            });
        }
        if($request->input('duration')){
            if($request->input('duration') > 60){
                $base->whereRaw('ABS(`finished_at`-`started_at`) > '.($request->input('duration') * 60));
            }else{
                $base->whereRaw('ABS(`finished_at`-`started_at`) < '.($request->input('duration') * 60));
            }
        }

        $timeframes = $base->where($where)->paginate(20);

        return view('admin.timeframes.index', compact('timeframes','users','projects','tasks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
