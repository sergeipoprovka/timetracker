<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'project_id'=>'required|integer',
            'name'=>'required|string'
        ]);

        $task = Task::create([
            'project_id'=>$request->project_id,
            'name'=>$request->name
        ]);

        if($task){
            return $task;
        }
    }
}
