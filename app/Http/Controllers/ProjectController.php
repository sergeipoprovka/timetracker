<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('projects.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'currentclient'=>'required|integer',
            'currentcategory'=>'required|integer',
            'name'=>'required|string',
            'estimation'=>'nullable|integer'
        ]);

        $project = Project::create([
            'team_id'=>auth()->user()->own_team->id,
            'category_id'=>$request->currentcategory,
            'client_id'=>$request->currentclient,
            'name'=>$request->name,
            'estimation'=>$request->estimation
        ]);

        if($project){
            return Project::with('client')->where('id','=',$project->id)->first();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $request->validate([
            'currentclient'=>'required|integer',
            'currentcategory'=>'required|integer',
            'name'=>'required|string',
            'estimation'=>'nullable|integer'
        ]);

        $project->team_id = auth()->user()->own_team->id;
        $project->category_id = $request->currentcategory;
        $project->client_id = $request->currentclient;
        $project->name = $request->name;
        $project->estimation = $request->estimation;

        if($project->save()){
            return Project::with('client')->where('id','=',$project->id)->first();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        if(Project::destroy($project->id)){
            return "1";
        }

        return "0";
    }
}










