<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TimeFrame;

class TimeframeController extends Controller
{
    public function store(Request $request){
        $timeframe = TimeFrame::create([
            'user_id'=>auth()->user()->id,
            'started_at'=>time()
        ]);

        if($timeframe){
            return $timeframe;
        }
    }

    public function update(Request $request, TimeFrame $timeframe)
    {
        $request->validate([
            'task_id' => 'nullable|integer',
            'finished_at' => 'nullable|integer'
        ]);

        if($request->task_id) $timeframe->task_id = $request->task_id;
        if($request->finished_at) $timeframe->finished_at = time();

        if($timeframe->save()){
            return $timeframe;
        }

    }
}









