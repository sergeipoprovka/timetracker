<?php

namespace App\Http\Middleware;

use Closure;

class PayedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check()){
            if(auth()->user()->subscription_active){
                return $next($request);
            }else{
                return redirect('/payment');
            }
        }
    }
}
